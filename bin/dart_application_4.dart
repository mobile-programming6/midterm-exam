import 'package:dart_application_4/dart_application_4.dart'
    as dart_application_4;

import 'dart:io';

List<String> infix = [];
List<String> postfix = [];
List<String> operator = [];
List<int> ans = [];

void main(List<String> arguments) {
  print("input expression : ");
  var input = stdin.readLineSync()!;

  InputList(input);
  print("Infix :");
  print(infix);
  infixToPostfix(infix);
  print("Postfix :");
  print(postfix);
  calculate(postfix);
  print("Result :");
  print(ans);
}

void calculate(List<String> opera) {
  for (var i = 0; i < postfix.length; i++) {
    if (opera[i] == "+" ||
        opera[i] == "-" ||
        opera[i] == "*" ||
        opera[i] == "/" ||
        opera[i] == "^") {
      int r = ans.removeLast();
      int l = ans.removeLast();
      int sum = 1;
      switch (opera[i]) {
        case "+":
          ans.add(l + r);
          break;
        case "-":
          ans.add(l - r);
          break;
        case "*":
          ans.add(l * r);
          break;
        case "/":
          ans.add(l ~/ r);
          break;
        case "^":
          for (int i = 0; i < r; i++) {
            sum *= l;
          }
          ans.add(sum);
          break;
      }
    } else {
      ans.add(int.parse(opera[i]));
    }
  }
}

//post
void infixToPostfix(List<String> opera) {
  for (var i = 0; i < infix.length; i++) {
    if (opera[i] == "+" ||
        opera[i] == "-" ||
        opera[i] == "*" ||
        opera[i] == "/" ||
        opera[i] == "^") {
      while (!operator.isEmpty &&
          !(operator.last == "(") &&
          (opera[i] == "+" ||
              opera[i] == "-" && operator.last == "*" ||
              operator.last == "/" ||
              operator.last == "^")) {
        postfix.add(operator.last);
        operator.removeLast();
      }
      while (!operator.isEmpty &&
          !(operator.last == "(") &&
          (opera[i] == "*" ||
              opera[i] == "/" && operator.last == "*" ||
              operator.last == "/" ||
              operator.last == "^")) {
        postfix.add(operator.last);
        operator.removeLast();
      }
      operator.add(opera[i]);
    } else if (opera[i] == "(") {
      operator.add(opera[i]);
    } else if (opera[i] == ")") {
      while (!(operator.last == "(")) {
        postfix.add(operator.last);
        operator.removeLast();
      }
      operator.removeLast();
    } else {
      postfix.add(opera[i]);
    }
  }
  while (!operator.isEmpty) {
    postfix.add(operator.last);
    operator.removeLast();
  }
}

//in
List<String> InputList(var input) {
  infix = input.split(' ');
  return infix;
}
